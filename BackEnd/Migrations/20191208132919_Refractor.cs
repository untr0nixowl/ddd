﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BackEnd.Migrations
{
    public partial class Refractor : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ConferenceID",
                table: "Speakers",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Attendees",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    FirstName = table.Column<string>(maxLength: 200, nullable: false),
                    LastName = table.Column<string>(maxLength: 200, nullable: false),
                    UserName = table.Column<string>(maxLength: 200, nullable: false),
                    EmailAddress = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Attendees", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Conferences",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Conferences", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Tags",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(maxLength: 32, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tags", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "ConferenceAttendee",
                columns: table => new
                {
                    ConferenceID = table.Column<int>(nullable: false),
                    AttendeeID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConferenceAttendee", x => new { x.ConferenceID, x.AttendeeID });
                    // table.ForeignKey(
                    //     name: "FK_ConferenceAttendee_Attendees_AttendeeID",
                    //     column: x => x.AttendeeID,
                    //     principalTable: "Attendees",
                    //     principalColumn: "ID",
                    //     onDelete: ReferentialAction.Cascade);
                    // table.ForeignKey(
                    //     name: "FK_ConferenceAttendee_Conferences_ConferenceID",
                    //     column: x => x.ConferenceID,
                    //     principalTable: "Conferences",
                    //     principalColumn: "ID",
                    //     onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Tracks",
                columns: table => new
                {
                    TrackID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ConferenceID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tracks", x => x.TrackID);
                    // table.ForeignKey(
                    //     name: "FK_Tracks_Conferences_ConferenceID",
                    //     column: x => x.ConferenceID,
                    //     principalTable: "Conferences",
                    //     principalColumn: "ID",
                    //     onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Sessions",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ConferenceID = table.Column<int>(nullable: false),
                    Title = table.Column<string>(maxLength: 200, nullable: false),
                    Abstract = table.Column<string>(maxLength: 4000, nullable: true),
                    StartTime = table.Column<DateTimeOffset>(nullable: true),
                    EndTime = table.Column<DateTimeOffset>(nullable: true),
                    TrackID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sessions", x => x.ID);
                    // table.ForeignKey(
                    //     name: "FK_Sessions_Conferences_ConferenceID",
                    //     column: x => x.ConferenceID,
                    //     principalTable: "Conferences",
                    //     principalColumn: "ID",
                    //     onDelete: ReferentialAction.Cascade);
                    // table.ForeignKey(
                    //     name: "FK_Sessions_Tracks_TrackID",
                    //     column: x => x.TrackID,
                    //     principalTable: "Tracks",
                    //     principalColumn: "TrackID",
                    //     onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SessionAttendee",
                columns: table => new
                {
                    SessionID = table.Column<int>(nullable: false),
                    AttendeeID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SessionAttendee", x => new { x.SessionID, x.AttendeeID });
                    // table.ForeignKey(
                    //     name: "FK_SessionAttendee_Attendees_AttendeeID",
                    //     column: x => x.AttendeeID,
                    //     principalTable: "Attendees",
                    //     principalColumn: "ID",
                    //     onDelete: ReferentialAction.Cascade);
                    // table.ForeignKey(
                    //     name: "FK_SessionAttendee_Sessions_SessionID",
                    //     column: x => x.SessionID,
                    //     principalTable: "Sessions",
                    //     principalColumn: "ID",
                    //     onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SessionSpeaker",
                columns: table => new
                {
                    SessionID = table.Column<int>(nullable: false),
                    SpeakerID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SessionSpeaker", x => new { x.SessionID, x.SpeakerID });
                    // table.ForeignKey(
                    //     name: "FK_SessionSpeaker_Sessions_SessionID",
                    //     column: x => x.SessionID,
                    //     principalTable: "Sessions",
                    //     principalColumn: "ID",
                    //     onDelete: ReferentialAction.Cascade);
                    // table.ForeignKey(
                    //     name: "FK_SessionSpeaker_Speakers_SpeakerID",
                    //     column: x => x.SpeakerID,
                    //     principalTable: "Speakers",
                    //     principalColumn: "ID",
                    //     onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SessionTag",
                columns: table => new
                {
                    SessionID = table.Column<int>(nullable: false),
                    TagID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SessionTag", x => new { x.SessionID, x.TagID });
                });

            migrationBuilder.CreateIndex(
                name: "IX_Speakers_ConferenceID",
                table: "Speakers",
                column: "ConferenceID");

            migrationBuilder.CreateIndex(
                name: "IX_Attendees_UserName",
                table: "Attendees",
                column: "UserName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ConferenceAttendee_AttendeeID",
                table: "ConferenceAttendee",
                column: "AttendeeID");

            migrationBuilder.CreateIndex(
                name: "IX_SessionAttendee_AttendeeID",
                table: "SessionAttendee",
                column: "AttendeeID");

            migrationBuilder.CreateIndex(
                name: "IX_Sessions_ConferenceID",
                table: "Sessions",
                column: "ConferenceID");

            migrationBuilder.CreateIndex(
                name: "IX_Sessions_TrackID",
                table: "Sessions",
                column: "TrackID");

            migrationBuilder.CreateIndex(
                name: "IX_SessionSpeaker_SpeakerID",
                table: "SessionSpeaker",
                column: "SpeakerID");

            migrationBuilder.CreateIndex(
                name: "IX_SessionTag_TagID",
                table: "SessionTag",
                column: "TagID");

            migrationBuilder.CreateIndex(
                name: "IX_Tracks_ConferenceID",
                table: "Tracks",
                column: "ConferenceID");

            // migrationBuilder.AddForeignKey(
            //     name: "FK_Speakers_Conferences_ConferenceID",
            //     table: "Speakers",
            //     column: "ConferenceID",
            //     principalTable: "Conferences",
            //     principalColumn: "ID",
            //     onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Speakers_Conferences_ConferenceID",
                table: "Speakers");

            migrationBuilder.DropTable(
                name: "ConferenceAttendee");

            migrationBuilder.DropTable(
                name: "SessionAttendee");

            migrationBuilder.DropTable(
                name: "SessionSpeaker");

            migrationBuilder.DropTable(
                name: "SessionTag");

            migrationBuilder.DropTable(
                name: "Attendees");

            migrationBuilder.DropTable(
                name: "Sessions");

            migrationBuilder.DropTable(
                name: "Tags");

            migrationBuilder.DropTable(
                name: "Tracks");

            migrationBuilder.DropTable(
                name: "Conferences");

            migrationBuilder.DropIndex(
                name: "IX_Speakers_ConferenceID",
                table: "Speakers");

            migrationBuilder.DropColumn(
                name: "ConferenceID",
                table: "Speakers");
        }
    }
}
