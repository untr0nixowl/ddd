using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BackEnd.Data;

namespace BackEnd.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SessionsController : ControllerBase
    {
        private readonly ApplicationDbContext _db;

        public SessionsController(ApplicationDbContext db)
        {
            _db = db;
        }

        // GET: api/Sessions
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ConferenceDTO.SessionResponse>>> GetSessions()
        {
            var sessions = await _db.Sessions.AsNoTracking().Include(s => s.Track).Include(s => s.SessionSpeakers).ThenInclude(ss => ss.Speaker).Include(s => s.SessionTags).ThenInclude(st => st.Tag).Select(m => m.MapSessionResponse()).ToListAsync();
            return sessions;
        }

        // GET: api/Sessions/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ConferenceDTO.SessionResponse>> GetSession(int id)
        {
           var session = await _db.Sessions.AsNoTracking().Include(s => s.Track).Include(s => s.SessionSpeakers).ThenInclude(ss => ss.Speaker).Include(s => s.SessionTags).ThenInclude(st => st.Tag).SingleOrDefaultAsync(s => s.ID == id);

            if (session == null)
            {
                return NotFound();
            }
            var response = session.MapSessionResponse();
            return response;
        }

        // PUT: api/Sessions/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSession(int id, ConferenceDTO.Session input)
        {
            var session = await _db.Sessions.FindAsync(id);
           if(session == null)
                return NotFound();

           session.ID = input.ID;
           session.Title = input.Title;
           session.Abstract = input.Abstract;
           session.StartTime = input.StartTime;
           session.EndTime = input.EndTime;
           session.TrackID = input.TrackID;
           session.ConferenceID = input.ConferenceID;

           await _db.SaveChangesAsync();
            return NoContent();
        }

        // POST: api/Sessions
        [HttpPost]
        public async Task<ActionResult<ConferenceDTO.SessionResponse>> PostSession(ConferenceDTO.Session input)
        {
            var session = new Data.Session{
                Title = input.Title,
                ConferenceID = input.ConferenceID,
                StartTime = input.StartTime,
                EndTime = input.EndTime,
                Abstract = input.Abstract,
                TrackID = input.TrackID
            };
            _db.Sessions.Add(session);
            await _db.SaveChangesAsync();
            var result = session.MapSessionResponse();
            return CreatedAtAction(nameof(GetSession), new { id = session.ID }, result);
        }

        // DELETE: api/Sessions/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ConferenceDTO.SessionResponse>> DeleteSession(int id)
        {
            var session = await _db.Sessions.FindAsync(id);
            if (session == null)
            {
                return NotFound();
            }

            _db.Sessions.Remove(session);
            await _db.SaveChangesAsync();

            return session.MapSessionResponse();
        }

        private bool SessionExists(int id)
        {
            return _db.Sessions.Any(e => e.ID == id);
        }
    }
}
