﻿using BackEnd.Data;
using ConferenceDTO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using System.Linq;
using System.Threading.Tasks;

namespace BackEnd.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class SearchController : ControllerBase
    {
        private ApplicationDbContext _db;

        public SearchController(ApplicationDbContext db)
        {
            _db = db;
        }

        [HttpPost]
        public async Task<IActionResult> Search(SearchTerm term)
        {
            var query = term.Query;
            var sessionResult = await _db.Sessions.AsNoTracking()
                .Include(s => s.Track)
                .Include(s => s.SessionSpeakers)
                .ThenInclude(ss => ss.Speaker)
                .Where(s => s.Title.Contains(query)
                            || s.Track.Name.Contains(query)).ToListAsync();

            var speakerResult = await _db.Speakers.AsNoTracking()
                .Include(s => s.SessionSpeakers)
                .ThenInclude(s => s.Session)
                .Where(s => s.Name.Contains(query) || s.Bio.Contains(query)).ToListAsync();

            var results = sessionResult.Select(s => new SearchResult
            {
                Type = SearchResultType.Session,
                Value = JObject.FromObject(s.MapSessionResponse())
            }
            ).Concat(speakerResult.Select(s => new SearchResult
            {
                Type = SearchResultType.Speaker,
                Value = JObject.FromObject(s.MapSpeakerResponse())
            }));


            return Ok(results);

        }


    }
}