using BackEnd.Data;
using ConferenceDTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace BackEnd.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AttendeesController : ControllerBase
    {
        private readonly ApplicationDbContext _db;

        public AttendeesController(ApplicationDbContext db)
        {
            _db = db;
        }

        // GET: api/Attendees/5
        [HttpGet("{id}")]
        public async Task<ActionResult<AttendeeResponse>> GetAttendee(string id)
        {
            var attendee = await _db.Attendees.Include(a => a.SessionAttendees).ThenInclude(s => s.Session).SingleOrDefaultAsync(a => a.UserName == id);
            if (attendee == null)
                return NotFound();
            var result = attendee.MapAttendeeResponse();
            return result;
        }

        // PUT: api/Attendees/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAttendee(int id, ConferenceDTO.Attendee attendee)
        {
            if (id != attendee.ID)
            {
                return BadRequest();
            }

            _db.Entry(attendee).State = EntityState.Modified;

            try
            {
                await _db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AttendeeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Attendees
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public async Task<ActionResult<AttendeeResponse>> PostAttendee(ConferenceDTO.Attendee input)
        {
            var existingAttendee = await _db.Attendees.Where(a => a.UserName == input.UserName).FirstOrDefaultAsync();
            if (existingAttendee != null)
                return Conflict(input);
            var attendee = new Data.Attendee
            {
                FirstName = input.FirstName,
                LastName = input.LastName,
                UserName = input.UserName,
                EmailAddress = input.EmailAddress
            };
            _db.Attendees.Add(attendee);
            await _db.SaveChangesAsync();
            var result = attendee.MapAttendeeResponse();

            return CreatedAtAction(nameof(GetAttendee), new { id = attendee.ID }, result);
        }

        [HttpPost("{username}/session/{sessionId:int}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public async Task<ActionResult<AttendeeResponse>> AddSession(string username, int sessionId)
        {
            var attendee = await _db.Attendees.Include(a => a.SessionAttendees).ThenInclude(sa => sa.Session).Include(a => a.ConferenceAttendees).ThenInclude(ca => ca.Conference).SingleOrDefaultAsync(a => a.UserName == username);
            if (attendee == null)
                return NotFound();
            var session = await _db.Sessions.FindAsync(sessionId);
            if (session == null)
                return BadRequest();
            attendee.SessionAttendees.Add(new SessionAttendee
            {
                AttendeeID = attendee.ID,
                SessionID = sessionId

            });
            await _db.SaveChangesAsync();
            var result = attendee.MapAttendeeResponse();
            return result;
        }

        [HttpDelete("{username}/session/{sessionId:int}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> RemoveSession(string username, int sessionId)
        {
            var attendee = await _db.Attendees.Include(a => a.SessionAttendees).SingleOrDefaultAsync(a => a.UserName == username);
            if (attendee == null)
                return NotFound();
            var session = await _db.Sessions.FindAsync(sessionId);
            if (session == null)
                return BadRequest();

            var sessionAttendee = attendee.SessionAttendees.FirstOrDefault(sa => sa.SessionID == sessionId);
            attendee.SessionAttendees.Remove(sessionAttendee);
            await _db.SaveChangesAsync();
            return NoContent();
        }


        private bool AttendeeExists(int id)
        {
            return _db.Attendees.Any(e => e.ID == id);
        }
    }
}
