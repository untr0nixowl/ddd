using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BackEnd.Data;
using ConferenceDTO;
using System.ComponentModel.DataAnnotations;

namespace BackEnd.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ConferencesController : ControllerBase
    {
        private readonly ApplicationDbContext _db;

        public ConferencesController(ApplicationDbContext db)
        {
            _db = db;
        }

        // GET: api/Conferences
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ConferenceDTO.ConferenceResponse>>> GetConferences()
        {
            return await _db.Conferences.AsNoTracking().Select(s => new ConferenceResponse { ID = s.ID,Name = s.Name }).ToListAsync();
        }

        // GET: api/Conferences/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ConferenceDTO.ConferenceResponse>> GetConference(int id)
        {
            var conference = await _db.FindAsync<Data.Conference>(id);

            if (conference == null)
            {
                return NotFound();
            }
            var result = new ConferenceResponse
            {
                ID = conference.ID,
                Name = conference.Name
            };

            return result;
        }

        [HttpPost("upload")]
        [Consumes("multipart/form-data")]
        public async Task<IActionResult> UplodaConference([Required,FromForm]string conferenceName,[FromForm] ConferenceFormat format,IFormFile file )
        {
            var loader = GetLoader(format);
            using (var stream = file.OpenReadStream())
            {
                await loader.LoadDataAsync(conferenceName,stream,_db);

            }
            await _db.SaveChangesAsync();
            return Ok();
        }
        
        // PUT: api/Conferences/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutConference(int id, ConferenceDTO.Conference input)
        {
            var conference = await _db.FindAsync<Data.Conference>(id);
            if(conference == null)
                return NotFound();
            
            conference.Name = input.Name;
            await _db.SaveChangesAsync();

            return NoContent();
        }

        // POST: api/Conferences
        [HttpPost]
        public async Task<ActionResult<ConferenceResponse>> CreateConference(ConferenceDTO.Conference input)
        {
            var conference = new Data.Conference
            {
                Name = input.Name
            };
            _db.Conferences.Add(conference);
            await _db.SaveChangesAsync();
            var result = new ConferenceResponse
            {
                ID = conference.ID,
                Name = conference.Name
            };
            return CreatedAtAction(nameof(GetConference), new { id = conference.ID }, result);
        }

        // DELETE: api/Conferences/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ConferenceResponse>> DeleteConference(int id)
        {
            var conference = await _db.FindAsync<Data.Conference>(id);
            if (conference == null)
            {
                return NotFound();
            }

            _db.Conferences.Remove(conference);
            await _db.SaveChangesAsync();
            var result = new ConferenceResponse
            {
                ID = conference.ID,
                Name = conference.Name
            };
            return result;
        }

        private bool ConferenceExists(int id)
        {
            return _db.Conferences.Any(e => e.ID == id);
        }
        private static DataLoader GetLoader(ConferenceFormat format)
        {
            if (format == ConferenceFormat.Sessionize)
            {
                return new SessionizeLoader();
            }
            return new DevIntersectionLoader();
        }
        public enum ConferenceFormat
        {
            Sessionize,
            DevIntersections
        }
    }
}
