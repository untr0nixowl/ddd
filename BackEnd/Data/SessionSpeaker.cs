using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackEnd.Data
{
    public class SessionSpeaker
    {
        public int SessionID { get; set; }
        public Session Session {get; set; }
        public int SpeakerID { get; set;  }
        public Speaker Speaker { get; set; }
    }
}